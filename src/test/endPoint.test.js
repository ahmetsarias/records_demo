process.env.SSL_DISABLED = 1
const { describe, before, after, beforeEach, afterEach, it } = require('mocha')
const sinon = require('sinon')
// const { expect } = require('chai')
const request = require('supertest')
const validPostBody = { startDate: '2020-01-01', endDate: '2020-01-02', minCount: 10, maxCount: 20 }

describe('POST /api/records', function () {
  let mongoPool
  let mongoPoolStub
  let app
  let server
  let recordsService
  let recordsServiceStub

  before(async () => {
    // delete require cache in order to reset modules between tests
    delete require.cache[require.resolve('../db/mongoPool')]
    delete require.cache[require.resolve('../service/recordsService')]

    mongoPool = require('../db/mongoPool')
    mongoPoolStub = sinon.stub(mongoPool, 'initPool')
    recordsService = require('../service/recordsService')
    mongoPoolStub.callsArg(0)
    app = require('../app').app
    server = require('../app').server
  })

  after(async () => {
    sinon.restore()
    server.close()
  })

  beforeEach(async () => {
    recordsServiceStub = sinon.stub(recordsService, 'filterRecords')
  })

  afterEach(async () => {
    recordsServiceStub.restore()
  })

  it('responds with bad request', function (done) {
    request(app)
      .post('/api/records')
      .set('Accept', 'application/json')
      .expect(400, done)
  })

  it('responds with 200', function (done) {
    recordsServiceStub.resolves([])
    request(app)
      .post('/api/records')
      .send(validPostBody)
      .set('Accept', 'application/json')
      .expect(200, done)
  })

  it('responds with 500', function (done) {
    recordsServiceStub.rejects(Error('test'))
    request(app)
      .post('/api/records')
      .send(validPostBody)
      .set('Accept', 'application/json')
      .expect(500, done)
  })
})
