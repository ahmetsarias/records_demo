const TestDbHelper = require('../testUtil/testDbHelper')
const { describe, before, after, afterEach, it } = require('mocha')
const { expect } = require('chai')

let recordsService
const dbHelper = new TestDbHelper()
const collectionName = 'records'

describe('Record Service Test', () => {
  before(async () => {
    await dbHelper.start()
    process.env.MONGODB_URL = await dbHelper.getConnectionString()
    // delete require cache in order to reset modules between tests
    delete require.cache[require.resolve('../../db/mongoPool')]
    delete require.cache[require.resolve('../../service/recordsService')]
    recordsService = require('../../service/recordsService')
  })

  after(async () => {
    await dbHelper.stop()
    require('../../db/mongoPool').closeConnection()
  })

  afterEach(async () => {
    await dbHelper.cleanup()
  })

  it('should return empty array when collection empty', async () => {
    const result = await recordsService.filterRecords({ startDate: '2015-06-03', endDate: '2015-06-04', minCount: 2700, maxCount: 6000 })
    expect(result.length).to.equal(0)
  })

  it('should return empty result when date filter does not fit', async () => {
    const sampleDoc = { _id: 'id1', key: 'testKey1', createdAt: new Date('2020-01-03'), counts: [10, 20, 30] }
    await dbHelper.createDoc(collectionName, sampleDoc)
    const result = await recordsService.filterRecords({ startDate: '2020-01-01', endDate: '2020-01-02', minCount: 50, maxCount: 70 })
    expect(result.length).to.equal(0)
  })

  it('should return empty result when count filter does not fit', async () => {
    const sampleDoc = { _id: 'id1', key: 'testKey1', createdAt: new Date('2020-01-01'), counts: [10, 20, 30] }
    await dbHelper.createDoc(collectionName, sampleDoc)
    const result = await recordsService.filterRecords({ startDate: '2020-01-01', endDate: '2020-01-02', minCount: 50, maxCount: 59 })
    expect(result.length).to.equal(0)
  })

  it('should return valid result when filter fits', async () => {
    const sampleDoc = { _id: 'id1', key: 'testKey1', createdAt: new Date('2020-01-01'), counts: [10, 20, 30] }
    await dbHelper.createDoc(collectionName, sampleDoc)
    const result = await recordsService.filterRecords({ startDate: '2020-01-01', endDate: '2020-01-05', minCount: 50, maxCount: 70 })
    expect(result.length).to.equal(1)
    expect(result[0].key).to.equal(sampleDoc.key)
    expect(result[0].createdAt.toISOString()).to.equal(sampleDoc.createdAt.toISOString())
    expect(result[0].totalCount).to.equal(sampleDoc.counts.reduce((a, b) => a + b, 0))
  })
})
