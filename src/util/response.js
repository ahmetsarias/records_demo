const BAD_REQUEST = {
  code: 400,
  msg: 'Invalid Request'
}

const SUCCESS = {
  code: 0,
  msg: 'success'
}

const UNEXPECTED_ERROR = {
  code: 500,
  msg: 'Unexpected error occurred'
}

module.exports = { SUCCESS, BAD_REQUEST, UNEXPECTED_ERROR }
