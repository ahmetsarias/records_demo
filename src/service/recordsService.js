const MongoPool = require('../db/mongoPool')

/**
 * Filters records by given parameters
 * @param startDate
 * @param endDate
 * @param minCount
 * @param maxCount
 * @returns {Promise<Array>}
 */
const filterRecords = async ({ startDate, endDate, minCount, maxCount }) => new Promise((resolve, reject) => {
  // The default timezone for node.js new Date() object is UTC
  startDate = new Date(startDate)
  endDate = new Date(endDate)

  MongoPool.getInstance((db) => {
    const pipeline = [
      {
        $match: {
          createdAt: {
            $gte: startDate,
            $lt: endDate
          }
        }
      },
      {
        $project: {
          _id: 0,
          key: '$key',
          createdAt: '$createdAt',
          totalCount: { $sum: '$counts' }

        }
      },
      {
        $match: {
          totalCount: {
            $gte: minCount,
            $lt: maxCount
          }
        }
      }
    ]

    db.collection('records').aggregate(pipeline).toArray((err, result) => {
      if (err) reject(err)
      else {
        resolve(result)
      }
    })
  })
})

module.exports = { filterRecords }
