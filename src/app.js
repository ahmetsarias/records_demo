const express = require('express')
const https = require('https')
const fs = require('fs')
const recordsRestApi = require('./api/records')
const mongoPool = require('./db/mongoPool')
const bodyParser = require('body-parser')
const { ValidationError } = require('express-json-validator-middleware')
const { BAD_REQUEST, UNEXPECTED_ERROR } = require('./util/response')

const PORT = process.env.PORT || 80
const SSL_DISABLED = process.env.SSL_DISABLED || false
const SSL_KEY = process.env.SSL_KEY || '/config/key.crt'
const SSL_CERT = process.env.SSL_CERT || '/config/cert.crt'

const app = express()

app.use(bodyParser.json())
app.use('/api/records', recordsRestApi)

// Error handler middleware
app.use(function (err, req, res, next) {
  if (err instanceof ValidationError) {
    const response = BAD_REQUEST
    response.details = err.validationErrors
    res.status(400).send(response)
  } else {
    console.error(err)
    res.status(500).send(UNEXPECTED_ERROR)
  }
  next()
})

let server
mongoPool.initPool(() => {
  if (!SSL_DISABLED) {
    server = https.createServer({
      key: fs.readFileSync(SSL_KEY),
      cert: fs.readFileSync(SSL_CERT)
    }, app)
    server.listen(PORT, () => {
      console.log(`HTTPS Server running on port ${PORT}`)
    })
  } else {
    server = app.listen(PORT)
    console.log(`HTTP Server listening on port: ${PORT}`)
  }
})

module.exports = { app, server }
