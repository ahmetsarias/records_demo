module.exports = {
  type: 'object',
  required: ['startDate', 'endDate', 'minCount', 'maxCount'],
  properties: {
    startDate: {
      type: 'string',
      format: 'date'
    },
    endDate: {
      type: 'string',
      format: 'date'
    },
    minCount: {
      type: 'integer'
    },
    maxCount: {
      type: 'integer'
    }
  }
}
