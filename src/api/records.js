const Router = require('express').Router
const recordsService = require('../service/recordsService')
const { Validator } = require('express-json-validator-middleware')
const recordsSchema = require('./validation/recordsSchema')
const { SUCCESS } = require('../util/response')

const router = new Router()
const validator = new Validator({ allErrors: true })

/**
 * Router for records filter endpoint
 */
router.post('/', validator.validate({ body: recordsSchema }), async (req, res, next) => {
  recordsService.filterRecords(req.body).then(result => {
    const response = SUCCESS
    response.records = result
    res.send(response)
  }).catch((err) => {
    next(err)
  })
})

module.exports = router
