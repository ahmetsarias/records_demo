const MongoClient = require('mongodb').MongoClient
const url = process.env.MONGODB_URL

class MongoPool {
  constructor () {
    this.pDb = null
    this.client = null
  }

  initPool (cb) {
    const self = this
    if (!url) {
      throw Error('Connection String must be specified')
    }
    MongoClient.connect(url, function (err, client) {
      if (err) throw err
      self.client = client
      self.pDb = client.db()
      self.pDb.on('close', () => { console.error('Db connection lost') })
      self.pDb.on('reconnect', () => { console.info('Reconnected to Db') })
      if (cb && typeof (cb) === 'function') { cb(self.pDb) }
    })
    return MongoPool
  }

  getInstance (cb) {
    if (!this.pDb) {
      this.initPool(cb)
    } else {
      if (cb && typeof (cb) === 'function') { cb(this.pDb) }
    }
  }

  closeConnection () {
    this.client.close()
  }
}

module.exports = new MongoPool()
