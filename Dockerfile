FROM alpine:3.8
RUN apk add --no-cache npm
WORKDIR /build

COPY package*.json ./
COPY . .

RUN npm i --prod
ENV NODE_ENV=production
EXPOSE 80
CMD [ "node", "src/app.js" ]
