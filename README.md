
# records demo

## Instructions

 - Set MONGODB_URL in ".env" file.
 - Build docker image `docker build -t records_demo .`
 - Start docker-compose service `docker-compose up`